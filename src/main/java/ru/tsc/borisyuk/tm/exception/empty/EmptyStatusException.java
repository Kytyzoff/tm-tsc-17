package ru.tsc.borisyuk.tm.exception.empty;

import ru.tsc.borisyuk.tm.exception.AbstractException;

public class EmptyStatusException extends AbstractException {

    public EmptyStatusException() {
        super("Error. Status  cannot take in an empty String or null value.");
    }

}
